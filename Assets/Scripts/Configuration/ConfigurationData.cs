﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// A container for the configuration data
/// </summary>
public class ConfigurationData
{
    #region Fields

    const string ConfigurationDataFileName = "ConfigurationData.csv";

    // configuration data
    static float paddleMoveUnitsPerSecond = 10;
    static float ballImpulseForce = 200;
    static float ballDeathTime = 10;
    static float delayMovingSeconds = 1;
    static float minSpawnSeconds = 5;
    static float maxSpawnSeconds = 10;
    static int standardBlockPoints = 5;
    static int bonusBlockPoints = 10;
    static int pickupBlockPoints = 7;
    static int standardBlockProbability = 3;
    static int bonusBlockProbability = 4;
    static int freezerBlockProbability = 2;
    static int speedupBlockProbability = 1;
    static int ballsPerGame = 10;
    static float freezeDuration = 2;
    static float speedupDuration = 2;

    #endregion

    #region Properties

    /// <summary>
    /// Gets the paddle move units per second
    /// </summary>
    /// <value>paddle move units per second</value>
    public float PaddleMoveUnitsPerSecond
    {
        get { return paddleMoveUnitsPerSecond; }
    }

    /// <summary>
    /// Gets the impulse force to apply to move the ball
    /// </summary>
    /// <value>impulse force</value>
    public float BallImpulseForce
    {
        get { return ballImpulseForce; }
    }

    // Gets the time after which the ball disappears
    public float BallDeathTime
    {
        get { return ballDeathTime; }
    }

    // get the start moving delay
    public float DelayMovingSeconds
    {
        get { return delayMovingSeconds; }
    }

    // get the minimum time for random ball spawning
    public float MinSpawnSeconds
    {
        get { return minSpawnSeconds; }
    }

    // get the maximum time for random ball spawning
    public float MaxSpawnSeconds
    {
        get { return maxSpawnSeconds; }
    }

    // get points for standard block
    public int StandardBlockPoints
    {
        get { return standardBlockPoints; }
    }

    // get points for bonus block
    public int BonusBlockPoints
    {
        get { return bonusBlockPoints; }
    }

    // get points for bonus block
    public int PickupBlockPoints
    {
        get { return pickupBlockPoints; }
    }

    // get standard probability block
    public int StandardBlockProbability
    {
        get { return standardBlockProbability; }
    }

    // get bonus probability block
    public int BonusBlockProbability
    {
        get { return bonusBlockProbability; }
    }

    // get freezer probability block
    public int FreezerBlockProbability
    {
        get { return freezerBlockProbability; }
    }

    // get speedup probability block
    public int SpeedupBlockProbability
    {
        get { return speedupBlockProbability; }
    }

    // get number of balls per game
    public int BallsPerGame
    {
        get { return ballsPerGame; }
    }

    // get freeze effect duration
    public float FreezeDuration
    {
        get { return freezeDuration; }
    }

    // get speedup effect duration
    public float SpeedupDuration
    {
        get { return speedupDuration; }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// Reads configuration data from a file. If the file
    /// read fails, the object contains default values for
    /// the configuration data
    /// </summary>
    public ConfigurationData()
    {
        StreamReader input = null;
        try
        {
            input = File.OpenText(
                Path.Combine(Application.streamingAssetsPath, ConfigurationDataFileName));

            // read lines from file
            string[] names = input.ReadLine().Split(',');
            string[] values = input.ReadLine().Split(',');

            // set values to variables
            paddleMoveUnitsPerSecond = float.Parse(values[0]);
            ballImpulseForce = float.Parse(values[1]);
            ballDeathTime = float.Parse(values[2]);
            minSpawnSeconds = float.Parse(values[3]);
            maxSpawnSeconds = float.Parse(values[4]);
            standardBlockPoints = int.Parse(values[5]);
            bonusBlockPoints = int.Parse(values[6]);
            pickupBlockPoints = int.Parse(values[7]);
            standardBlockProbability = int.Parse(values[8]);
            bonusBlockProbability = int.Parse(values[9]);
            freezerBlockProbability = int.Parse(values[10]);
            speedupBlockProbability = int.Parse(values[11]);
            ballsPerGame = int.Parse(values[12]);
            freezeDuration = float.Parse(values[13]);
            speedupDuration = float.Parse(values[14]);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        finally
        {
            if (input != null)
            {
                input.Close();
            }
        }
    }

    #endregion
}
