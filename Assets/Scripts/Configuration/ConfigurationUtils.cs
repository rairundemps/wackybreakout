﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides access to configuration data
/// </summary>
public static class ConfigurationUtils
{
    #region Fields

    static ConfigurationData configurationData;

    #endregion

    #region Properties

    /// <summary>
    /// Gets the paddle move units per second
    /// </summary>
    /// <value>paddle move units per second</value>
    public static float PaddleMoveUnitsPerSecond
    {
        get { return configurationData.PaddleMoveUnitsPerSecond; }
    }

    // gets the ball impulse force
    public static float BallImpulseForce
    {
        get { return configurationData.BallImpulseForce; }
    }

    // gets the time after which the ball disappears
    public static float BallDeathTimer
    {
        get { return configurationData.BallDeathTime; }
    }

    // get the start moving delay
    public static float DelayMovingSeconds
    {
        get { return configurationData.DelayMovingSeconds; }
    }

    // get the minimum time for random ball spawning
    public static float MinSpawnSeconds
    {
        get { return configurationData.MinSpawnSeconds; }
    }

    // get the maximum time for random ball spawning
    public static float MaxSpawnSeconds
    {
        get { return configurationData.MaxSpawnSeconds; }
    }

    // get points for standard block
    public static int StandardBlockPoints
    {
        get { return configurationData.StandardBlockPoints; }
    }

    // get points for bonus block
    public static int BonusBlockPoints
    {
        get { return configurationData.BonusBlockPoints; }
    }

    // get points for pickup block
    public static int PickupBlockPoints
    {
        get { return configurationData.PickupBlockPoints; }
    }

    // get probability for standard block
    public static int StandardBlockProbability
    {
        get { return configurationData.StandardBlockProbability; }
    }

    // get probability for bonus block
    public static int BonusBlockProbability
    {
        get { return configurationData.BonusBlockProbability; }
    }

    // get probability for freezer block
    public static int FreezerBlockProbability
    {
        get { return configurationData.FreezerBlockProbability; }
    }

    // get probability for standard block
    public static int SpeedupBlockProbability
    {
        get { return configurationData.SpeedupBlockProbability; }
    }

    // get number of balls per game
    public static int BallsPerGame
    {
        get { return configurationData.BallsPerGame; }
    }

    // get freeze effect duration
    public static float FreezeDuration
    {
        get { return configurationData.FreezeDuration; }
    }

    // get speedup effect duration
    public static float SpeedupDuration
    {
        get { return configurationData.SpeedupDuration; }
    }

    #endregion

    /// <summary>
    /// Initializes the configuration utils
    /// </summary>
    public static void Initialize()
    {
        configurationData = new ConfigurationData();
    }
}
