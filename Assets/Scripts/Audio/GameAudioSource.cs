﻿using UnityEngine;

/// <summary>
/// An audio source for the entire game
/// </summary>
public class GameAudioSource : MonoBehaviour
{
    // Awake is called before start
    void Awake()
    {
        // make sure we only have one of this game object in the game
        if (!AudioManager.Initialized)
        {
            // initialize audio manager and persist audio source across
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            AudioManager.Initialize(audioSource);
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
