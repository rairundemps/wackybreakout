﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioClipName
{
    ButtonPressed,
    GameStarted,
    SpeedBlockDestroyed,
    FreezeBlockDestroyed,
    BallLost,
    GameOver
}
