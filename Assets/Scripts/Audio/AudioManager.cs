﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The audio manager
/// </summary>
public static class AudioManager
{
    static bool initialized = false;
    static AudioSource audioSource;
    static Dictionary<AudioClipName, AudioClip> audioClips =
        new Dictionary<AudioClipName, AudioClip>();

    /// <summary>
    /// Gets whether or not the audio manager has been initialized
    /// </summary>
    public static bool Initialized
    {
        get { return initialized; }
    }

    public static void Initialize(AudioSource source)
    {
        initialized = true;
        audioSource = source;
        audioClips.Add(AudioClipName.ButtonPressed,
            Resources.Load<AudioClip>("ButtonPressed"));
        audioClips.Add(AudioClipName.GameStarted,
            Resources.Load<AudioClip>("GameStarted"));
        audioClips.Add(AudioClipName.SpeedBlockDestroyed,
            Resources.Load<AudioClip>("SpeedBlockDestroyed"));
        audioClips.Add(AudioClipName.FreezeBlockDestroyed,
            Resources.Load<AudioClip>("FreezeBlockDestroyed"));
        audioClips.Add(AudioClipName.BallLost,
            Resources.Load<AudioClip>("BallLost"));
        audioClips.Add(AudioClipName.GameOver,
            Resources.Load<AudioClip>("GameOver"));
    }

    public static void Play(AudioClipName name)
    {
        audioSource.PlayOneShot(audioClips[name]);
    }
}
