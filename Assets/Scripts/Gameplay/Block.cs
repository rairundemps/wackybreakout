﻿using UnityEngine;
using UnityEngine.Events;

public class Block : MonoBehaviour
{
    // how many block worth
    protected int points;

    // points added event support
    PointsAdded pointsAdded = new PointsAdded();

    // block destroyed event support
    BlockDestroyedEvent blockDestroyedEvent = new BlockDestroyedEvent();

    // Start is called before the first frame update
    virtual protected void Start()
    {
        // set the invoker of the points added event
        EventManager.AddPointsAddedInvoker(this);

        // set the invoker of the block destroyed event
        EventManager.AddBlockDestroyedInvoker(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    virtual protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            pointsAdded.Invoke(points);
            blockDestroyedEvent.Invoke();
            Destroy(gameObject);
        }
    }

    public void AddPointsAddedListener(UnityAction<int> listener)
    {
        pointsAdded.AddListener(listener);
    }

    public void AddBlockDestroyedListener(UnityAction listener)
    {
        blockDestroyedEvent.AddListener(listener);
    }
}
