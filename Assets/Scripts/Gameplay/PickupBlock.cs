﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickupBlock : Block
{
    [SerializeField]
    Sprite[] pickupSprites = new Sprite[2];
    [SerializeField]
    PickupEffect pickupEffect;

    // freeze event support
    float freezeDuration;
    FreezerEffectActivated freezeActivated;

    // speedup event support
    float speedupDuration;
    SpeedupEffectActivated speedupActivated;

    public PickupEffect PickupEffect
    {
        set
        {
            pickupEffect = value;
            if (pickupEffect == PickupEffect.Freezer)
            {
                GetComponent<SpriteRenderer>().sprite = pickupSprites[0];
                freezeDuration = ConfigurationUtils.FreezeDuration;
                freezeActivated = new FreezerEffectActivated();
                EventManager.AddFreezeInvoker(this);
            }
            else if (pickupEffect == PickupEffect.Speedup)
            {
                GetComponent<SpriteRenderer>().sprite = pickupSprites[1];
                speedupDuration = ConfigurationUtils.SpeedupDuration;
                speedupActivated = new SpeedupEffectActivated();
                EventManager.AddSpeedupInvoker(this);
            }
        }
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        base.points = ConfigurationUtils.PickupBlockPoints;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // adds listener for the freeze event
    public void AddFreezerEffectListener(UnityAction<float> handler)
    {
        freezeActivated.AddListener(handler);
    }

    // adds listener for the speedup effect
    public void AddSpeedupEffectListener(UnityAction<float> handler)
    {
        speedupActivated.AddListener(handler);
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        // checks block effect and invokes appropriate event
        if (pickupEffect == PickupEffect.Freezer)
        {
            freezeActivated.Invoke(freezeDuration);
        }
        else if (pickupEffect == PickupEffect.Speedup)
        {
            speedupActivated.Invoke(speedupDuration);
        }
    }
}
