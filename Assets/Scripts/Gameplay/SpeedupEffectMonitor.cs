﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedupEffectMonitor : MonoBehaviour
{
    #region Fields

    // speedup monitor support
    Timer speedupTimer;

    #endregion

    #region Properties

    // get if speedup factor is active or not
    public bool IsActive
    {
        get { return speedupTimer.Running; }
    }

    // get what speedup factor is

    // get how much time of speedup is left
    public float TimeLeft
    {
        get { return speedupTimer.RemainingTime; }
    }

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        // timer and handler for tracking if speedup effect is up
        speedupTimer = gameObject.AddComponent<Timer>();
        EventManager.AddSpeedupListener(HandleSpeedupActivationEffect);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void HandleSpeedupActivationEffect(float speedupDuration)
    {
        if (speedupTimer.Running)
        {
            speedupTimer.IncreaseDuration(speedupDuration);
        }
        else
        {
            speedupTimer.Duration = speedupDuration;
            speedupTimer.Run();
        }
    }
}
