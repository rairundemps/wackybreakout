﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Paddle behaviour
/// </summary>
public class Paddle : MonoBehaviour
{
    const float BounceAngleHalfRange = 60 * Mathf.Deg2Rad;

    // created for efficiency
    Rigidbody2D rigidbody2D;
    Vector2 shift, newPosition;
    float colliderHalfWidth;

    // paddle freeze support
    bool isFrozen = false;
    Timer frozenTimer;

    // Start is called before the first frame update
    void Start()
    {
        // saved for efficiency
        rigidbody2D = GetComponent<Rigidbody2D>();

        // for keeping paddle in the playfield
        colliderHalfWidth = GetComponent<BoxCollider2D>().size.x * 0.5f;

        // add timer and handler for freeze effect
        frozenTimer = gameObject.AddComponent<Timer>();
        EventManager.AddFreezeListener(HandleFreezeEffect);

        // add listener for freeze time finished event
        frozenTimer.AddTimerFinishedListener(FreezeTimerFinishListener);
    }

    // called every fixed frame-rate frame
    private void FixedUpdate()
    {
        // check if it is frozen or not
        if (!isFrozen)
        {
            // check the input
            float horizontalInput = Input.GetAxis("Horizontal");
            if (horizontalInput != 0)
            {
                // calculate shifing and move paddle
                shift = new Vector2(ConfigurationUtils.PaddleMoveUnitsPerSecond *
                    horizontalInput * Time.fixedDeltaTime, 0);
                newPosition = rigidbody2D.position + shift;
                newPosition.x = CalculateClampedX(newPosition.x);
                rigidbody2D.MovePosition(newPosition);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // calculates clamped x value
    float CalculateClampedX(float positionX)
    {
        if (positionX - colliderHalfWidth < ScreenUtils.ScreenLeft)
        {
            positionX = ScreenUtils.ScreenLeft + colliderHalfWidth;
        }
        else if (positionX + colliderHalfWidth > ScreenUtils.ScreenRight)
        {
            positionX = ScreenUtils.ScreenRight - colliderHalfWidth;
        }
        return positionX;
    }

    /// <summary>
    /// Detects collision with a ball to aim the ball
    /// </summary>
    /// <param name="coll">collision info</param>
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Ball") && isTopCollision(coll))
        {
            // calculate new ball direction
            float ballOffsetFromPaddleCenter = transform.position.x -
                coll.transform.position.x;
            float normalizedBallOffset = ballOffsetFromPaddleCenter /
                colliderHalfWidth;
            float angleOffset = normalizedBallOffset * BounceAngleHalfRange;
            float angle = Mathf.PI / 2 + angleOffset;
            Vector2 direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

            // tell ball to set direction to new direction
            Ball ballScript = coll.gameObject.GetComponent<Ball>();
            ballScript.SetDirection(direction);
        }
    }

    // check if collision occurs at the top of the collider
    bool isTopCollision(Collision2D coll)
    {
        if (Mathf.Abs(coll.GetContact(0).point.y - coll.GetContact(1).point.y) > 0.05f)
        {
            return false;
        }
        return true;
    }


    // handle freeze of the block
    void HandleFreezeEffect(float freezeDuration)
    {
        AudioManager.Play(AudioClipName.FreezeBlockDestroyed);
        isFrozen = true;
        // check if timer is already running or not
        if (frozenTimer.Running)
        {
            // increase duration of the timer
            frozenTimer.IncreaseDuration(freezeDuration);
        }
        else
        {
            // set duration and run timer
            frozenTimer.Duration = freezeDuration;
            frozenTimer.Run();
        }
    }

    // handle freeze timer finishes
    void FreezeTimerFinishListener()
    {
        isFrozen = false;
        frozenTimer.Stop();
    }
}
