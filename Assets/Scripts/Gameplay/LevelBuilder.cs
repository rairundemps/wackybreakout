﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    [SerializeField]
    GameObject paddlePrefab;
    [SerializeField]
    GameObject standardBlockPrefab;
    [SerializeField]
    GameObject bonusBlockPrefab;
    [SerializeField]
    GameObject pickupBlockPrefab;

    // saved for efficiency
    int[] probabilities;
    int sumOfProbabilities;
    const int NumOfBlockRows = 3;

    // Start is called before the first frame update
    void Start()
    {
        // get current screen size
        ScreenUtils.Initialize();

        // instantiate paddle
        Instantiate(paddlePrefab);

        // get sum of probabilities
        probabilities = new int[4] {
        ConfigurationUtils.StandardBlockProbability,
        ConfigurationUtils.BonusBlockProbability,
        ConfigurationUtils.FreezerBlockProbability,
        ConfigurationUtils.SpeedupBlockProbability};
        ProbabilitiesSum();

        // instantiate temporary block to get size
        GameObject tempBlock = Instantiate(pickupBlockPrefab);
        Vector2 size = tempBlock.GetComponent<BoxCollider2D>().size;
        Destroy(tempBlock);

        // instantiate rows of blocks
        float screenWidth = ScreenUtils.ScreenRight - ScreenUtils.ScreenLeft;
        int countBlocks = (int)(screenWidth / size.x);
        Vector3 margins = new Vector3(
            ScreenUtils.ScreenLeft + (screenWidth - countBlocks * size.x) * 0.5f,
            ScreenUtils.ScreenTop * 0.6f, 0);
        Vector3 startPosition = margins + (Vector3)size * 0.5f;
        for (int i = 0; i < NumOfBlockRows; i++)
        {
            for (int j = 0; j < countBlocks; j++)
            {
                InstantiateBlock(startPosition + new Vector3(j * size.x, i * size.y, 0));
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ProbabilitiesSum()
    {
        int sum = 0;
        for (int i = 0; i < probabilities.Length; i++)
        {
            sum += probabilities[i];
        }
        sumOfProbabilities = sum;
    }

    void InstantiateBlock(Vector3 position)
    {
        // instantiate block based on probability
        int index = -1;
        int sum = 0;
        int number = Random.Range(1, sumOfProbabilities + 1);
        while (sum < number)
        {
            index++;
            sum += probabilities[index];
        }

        GameObject block;
        if (index == 0)
        {
            Instantiate(standardBlockPrefab, position, Quaternion.identity);
        }
        else if (index == 1)
        {
            Instantiate(bonusBlockPrefab, position, Quaternion.identity);
        }
        else if (index == 2)
        {
            block = Instantiate(pickupBlockPrefab, position, Quaternion.identity);
            block.GetComponent<PickupBlock>().PickupEffect = PickupEffect.Freezer;
        }
        else if (index == 3)
        {
            block = Instantiate(pickupBlockPrefab, position, Quaternion.identity);
            block.GetComponent<PickupBlock>().PickupEffect = PickupEffect.Speedup;
        }
    }
}
