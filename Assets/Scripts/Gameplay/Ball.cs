﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Ball behaviour
/// </summary>
public class Ball : MonoBehaviour
{
    // saved for efficiency
    Rigidbody2D rigidbody2D;
    float colliderHalfHeight;
    float delayMovingSeconds;

    // timer for delaying ball moving at the start
    // and death of ball after certain period of time
    Timer moveDelayTimer;
    Timer deathTimer;

    // timer and for speedup support
    Timer speedupTimer;
    const float speedMultiplicator = 1.5f;

    // decrease balls event support
    BallsDecreaseEvent ballsDecreaseEvent = new BallsDecreaseEvent();

    // ball dies event support
    BallDiesEvent ballDiesEvent = new BallDiesEvent();

    // Start is called before the first frame update
    void Start()
    {
        // get rigidbody2d component and collider radius
        rigidbody2D = GetComponent<Rigidbody2D>();
        colliderHalfHeight = gameObject.GetComponent<BoxCollider2D>().size.y * 0.5f;

        // add timer and listener for the speedup event
        speedupTimer = gameObject.AddComponent<Timer>();
        EventManager.AddSpeedupListener(HandleSpeedupEffect);

        // add move ball delay timer
        moveDelayTimer = gameObject.AddComponent<Timer>();
        moveDelayTimer.Duration = ConfigurationUtils.DelayMovingSeconds;
        moveDelayTimer.Run();

        // add death timer
        deathTimer = gameObject.AddComponent<Timer>();
        deathTimer.Duration = ConfigurationUtils.BallDeathTimer +
            ConfigurationUtils.DelayMovingSeconds;
        deathTimer.Run();

        // set the invoker of the balls decrease event
        EventManager.AddBallsDecreaseInvoker(this);

        // set the invoker of the ball dies event
        EventManager.AddBallDiesInvoker(this);

        // set listener for the finish of the move delay timer
        moveDelayTimer.AddTimerFinishedListener(MoveDelayTimerFinish);

        // set listener for the finish of the death timer
        deathTimer.AddTimerFinishedListener(DeathTimerListener);

        // set listener for the finish of the speedup timer
        speedupTimer.AddTimerFinishedListener(SpeedupTimerFinishListener);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDirection(Vector2 direction)
    {
        rigidbody2D.velocity = rigidbody2D.velocity.magnitude * direction;
    }

    private void OnBecameInvisible()
    {
        // check if object is really below the bottom line
        // so we don't spawn object while stopping the game
        bool isBelowBottom = gameObject.transform.position.y + colliderHalfHeight <
            ScreenUtils.ScreenBottom;

        // check if death timer is finished, so we don't spawn two balls,
        // because whe we destroying object it becomes invisible too
        if (!deathTimer.Finished && isBelowBottom)
        {
            ballsDecreaseEvent.Invoke();
            Destroy(gameObject);
        }
    }

    // adds speed to the ball and manage timer
    void HandleSpeedupEffect(float speedupDuration)
    {
        AudioManager.Play(AudioClipName.SpeedBlockDestroyed);
        if (speedupTimer.Running)
        {
            // increase duration of speedup timer
            speedupTimer.IncreaseDuration(speedupDuration);
        }
        else
        {
            // increase velocity and run timer for speedup
            rigidbody2D.velocity *= speedMultiplicator;
            speedupTimer.Duration = speedupDuration;
            speedupTimer.Run();
        }
    }

    // adds ball decrease event listener
    public void AddBallsDecreasedListener(UnityAction listener)
    {
        ballsDecreaseEvent.AddListener(listener);
    }

    // listener for move delay timer finish
    void MoveDelayTimerFinish()
    {
        // set start moving direction
        Vector2 direction = new Vector2(0, -1);
        Vector2 fullForce = direction * ConfigurationUtils.BallImpulseForce;
        if (EffectUtils.SpeedupIsActive)
        {
            fullForce *= speedMultiplicator;
            speedupTimer.Duration = EffectUtils.SpeedupTimeLeft;
            speedupTimer.Run();
        }
        rigidbody2D.AddForce(fullForce, ForceMode2D.Force);
    }

    // listener for death timer finish
    void DeathTimerListener()
    {
        // destroy old ball and spawn new
        Destroy(gameObject);
        ballDiesEvent.Invoke();
    }

    // listener for speedup timer finish
    void SpeedupTimerFinishListener()
    {
        // reduce speed
        rigidbody2D.velocity /= speedMultiplicator;
    }
}
