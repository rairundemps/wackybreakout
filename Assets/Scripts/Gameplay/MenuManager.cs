﻿using UnityEngine;
using UnityEngine.SceneManagement;

public static class MenuManager
{
    public static void GoToMenu(MenuName name)
    {
        switch (name)
        {
            case MenuName.Main:
                // go to start menu scene
                SceneManager.LoadScene("MainMenu");
                break;
            case MenuName.Gameplay:
                // go to gameplay scene
                SceneManager.LoadScene("Gameplay");
                break;
            case MenuName.Help:
                // go to help menu scene
                SceneManager.LoadScene("HelpMenu");
                break;
            case MenuName.Pause:
                // load pause menu
                if (GameObject.FindGameObjectWithTag("PauseMenu") == null &&
                    GameObject.FindGameObjectWithTag("GameOverMenu") == null)
                {
                    Object.Instantiate(Resources.Load("PauseMenu"));
                }
                break;
            case MenuName.GameOver:
                // load game over menu
                Object.Instantiate(Resources.Load("GameOverMenu"));
                break;
        }
    }
}
