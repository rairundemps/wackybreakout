﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    // print user's score support
    Text scoreText;
    const string ScoreMessage = "Your score is ";

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        scoreText = GameObject.FindWithTag("GameOverScore").GetComponent<Text>();
        scoreText.text = ScoreMessage + GameObject.FindWithTag("Score").GetComponent<Text>().text;
    }

    public void HandleQuitButtonOnClickEvent()
    {
        // unpause game, destroy menu, and go to main menu
        AudioManager.Play(AudioClipName.ButtonPressed);
        Time.timeScale = 1;
        Destroy(gameObject);
        MenuManager.GoToMenu(MenuName.Main);
    }
}
