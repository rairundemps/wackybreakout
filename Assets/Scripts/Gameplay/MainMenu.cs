﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void HandlePlayButtonOnClickEvent()
    {
        AudioManager.Play(AudioClipName.GameStarted);
        MenuManager.GoToMenu(MenuName.Gameplay);
    }

    public void HandleHelpButtonOnClickEvent()
    {
        AudioManager.Play(AudioClipName.ButtonPressed);
        MenuManager.GoToMenu(MenuName.Help);
    }

    public void HandleQuitButtonOnClickEvent()
    {
        AudioManager.Play(AudioClipName.ButtonPressed);
        Application.Quit();
    }
}
