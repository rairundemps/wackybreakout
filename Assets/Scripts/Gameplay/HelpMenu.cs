﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HelpMenu : MonoBehaviour
{
    public void HandleBackButtonOnClickEvent()
    {
        AudioManager.Play(AudioClipName.ButtonPressed);
        MenuManager.GoToMenu(MenuName.Main);
    }
}
