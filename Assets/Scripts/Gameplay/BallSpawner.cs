﻿using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject ballPrefab;

    Timer gapSpawnTimer;

    Vector2 minSpawnPoint;
    Vector2 maxSpawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        // get minimum and maximum points for overlap checking
        GameObject tempBall = Instantiate(ballPrefab);
        BoxCollider2D boxCollider2D = tempBall.GetComponent<BoxCollider2D>();
        Vector2 vector2Position = (Vector2) boxCollider2D.transform.position;
        Vector2 halfColliderSize = boxCollider2D.size * 0.5f;
        minSpawnPoint = vector2Position - halfColliderSize;
        maxSpawnPoint = vector2Position + halfColliderSize;
        Destroy(tempBall);

        // add and start spawn timer for ball
        gapSpawnTimer = gameObject.AddComponent<Timer>();
        SpawnBall();

        // add listener for balls spawn timer
        gapSpawnTimer.AddTimerFinishedListener(GapSpawnTimerListener);

        // add balls decrease and ball dies listeners for spawning balls
        EventManager.AddBallsDecreaseListener(SpawnBall);
        EventManager.AddBallDiesListener(SpawnBall);
    }

    // Update is called once per frame
    void Update()
    {

    }

    // spawning a new ball from prefab
    void SpawnBall()
    {
        if (Physics2D.OverlapArea(minSpawnPoint, maxSpawnPoint) == null)
        {
            StartTimer();
            Instantiate(ballPrefab);
        }
    }

    // starts timer with duration in a gap
    public void StartTimer()
    {
        gapSpawnTimer.Duration = Random.Range(ConfigurationUtils.MinSpawnSeconds,
            ConfigurationUtils.MaxSpawnSeconds) + ConfigurationUtils.DelayMovingSeconds;
        gapSpawnTimer.Run();
    }

    // handle ball spawn after timer finishes
    public void GapSpawnTimerListener()
    {
        SpawnBall();
    }
}
