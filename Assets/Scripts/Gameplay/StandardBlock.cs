﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardBlock : Block
{
    // sprites for standard blocks
    [SerializeField]
    Sprite[] spriteBlocks = new Sprite[4];

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        GetComponent<SpriteRenderer>().sprite = spriteBlocks[Random.Range(0, 4)];
        base.points = ConfigurationUtils.StandardBlockPoints;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
