﻿using System.Collections.Generic;
using UnityEngine.Events;

public static class EventManager
{
    #region Fields

    //save list of freeze event invokers and listeners
    static List<PickupBlock> freezeEffectInvokers = new List<PickupBlock>();
    static List<UnityAction<float>> freezeEffectListeners = new List<UnityAction<float>>();

    // save list of speedup event invokers and listeners
    static List<PickupBlock> speedupEffectInvokers = new List<PickupBlock>();
    static List<UnityAction<float>> speedupEffectListeners = new List<UnityAction<float>>();

    // save list of point added event invokers and listeners
    static List<Block> pointsAddedEventInvokers = new List<Block>();
    static List<UnityAction<int>> pointsAddedEventListeners = new List<UnityAction<int>>();

    // save list of ball decreased event invokers and listeners
    static List<Ball> ballsDescreasedInvokers = new List<Ball>();
    static List<UnityAction> ballsDecreasedListeners = new List<UnityAction>();

    // save list of ball dies event invokers and listeners
    static List<Ball> ballDiesInvokers = new List<Ball>();
    static List<UnityAction> ballDiesListeners = new List<UnityAction>();

    // save list of last ball lost event invokers and listeners
    static List<HUD> lastBallLostInvokers = new List<HUD>();
    static List<UnityAction> lastBallLostListeners = new List<UnityAction>();

    // save list of block destroyed event invokers and listeners
    static List<Block> blockDestroyedInvokers = new List<Block>();
    static List<UnityAction> blockDestroyedListeners = new List<UnityAction>();

    #endregion

    #region Public methods

    // adds invoker for freeze event
    public static void AddFreezeInvoker(PickupBlock invoker)
    {
        // add invoker to the list and all listeners to the invoker
        freezeEffectInvokers.Add(invoker);
        foreach (UnityAction<float> listener in freezeEffectListeners)
        {
            invoker.AddFreezerEffectListener(listener);
        }
    }

    // adds listener for freeze event
    public static void AddFreezeListener(UnityAction<float> handler)
    {
        // add listener to the list and to all invokers
        freezeEffectListeners.Add(handler);
        foreach (PickupBlock invoker in freezeEffectInvokers)
        {
            invoker.AddFreezerEffectListener(handler);
        }
    }

    // adds invoker for speedup event
    public static void AddSpeedupInvoker(PickupBlock invoker)
    {
        // add invoker to the list and all listeners to the invoker
        speedupEffectInvokers.Add(invoker);
        foreach (UnityAction<float> listener in speedupEffectListeners)
        {
            invoker.AddSpeedupEffectListener(listener);
        }
    }

    // adds listener for speedup event
    public static void AddSpeedupListener(UnityAction<float> handler)
    {
        // add listener to the list and to all invokers
        speedupEffectListeners.Add(handler);
        foreach (PickupBlock invoker in speedupEffectInvokers)
        {
            invoker.AddSpeedupEffectListener(handler);
        }
    }

    // adds invoker to the points added event
    public static void AddPointsAddedInvoker(Block invoker)
    {
        // add invoker to the list and all listeners to the invoker
        pointsAddedEventInvokers.Add(invoker);
        foreach (UnityAction<int> listener in pointsAddedEventListeners) {
            invoker.AddPointsAddedListener(listener);
        }
    }

    // adds listener to the points added event
    public static void AddPointsAddedListener(UnityAction<int> listener)
    {
        // add listener to the list and to all invokers
        pointsAddedEventListeners.Add(listener);
        foreach (Block invoker in pointsAddedEventInvokers)
        {
            invoker.AddPointsAddedListener(listener);
        }
    }

    // adds invoker to the balls decreased event
    public static void AddBallsDecreaseInvoker(Ball invoker)
    {
        // add invoker to the list and all listeners to the invoker
        ballsDescreasedInvokers.Add(invoker);
        foreach (UnityAction listener in ballsDecreasedListeners)
        {
            invoker.AddBallsDecreasedListener(listener);
        }
    }

    // adds listener to the balls decreased event
    public static void AddBallsDecreaseListener(UnityAction listener)
    {
        // add listener to the list and to all invokers
        ballsDecreasedListeners.Add(listener);
        foreach (Ball invoker in ballsDescreasedInvokers)
        {
            invoker.AddBallsDecreasedListener(listener);
        }
    }

    // adds invoker to ball dies event
    public static void AddBallDiesInvoker(Ball invoker)
    {
        // add invoker to the list and all listeners to the invoker
        ballDiesInvokers.Add(invoker);
        foreach (UnityAction listener in ballDiesListeners)
        {
            invoker.AddBallsDecreasedListener(listener);
        }
    }

    // adds listener to ball dies event
    public static void AddBallDiesListener(UnityAction listener)
    {
        // add listener to the list and to all invokers
        ballDiesListeners.Add(listener);
        foreach (Ball invoker in ballDiesInvokers)
        {
            invoker.AddBallsDecreasedListener(listener);
        }
    }

    // adds invoker to last ball lost event
    public static void AddLastBallLostInvoker(HUD invoker)
    {
        // add invoker to the list and all listeners to the invoker
        lastBallLostInvokers.Add(invoker);
        foreach (UnityAction listener in lastBallLostListeners)
        {
            invoker.AddLastBallLostEventListener(listener);
        }
    }

    // adds listener to last ball lost event
    public static void AddLastBallLostListener(UnityAction listener)
    {
        // add listener to the list and to all invokers
        lastBallLostListeners.Add(listener);
        foreach (HUD invoker in lastBallLostInvokers)
        {
            invoker.AddLastBallLostEventListener(listener);
        }
    }

    // adds invoker to the block destroyed event
    public static void AddBlockDestroyedInvoker(Block invoker)
    {
        // add invoker to the list and all listeners to the invoker
        blockDestroyedInvokers.Add(invoker);
        foreach (UnityAction listener in blockDestroyedListeners)
        {
            invoker.AddBlockDestroyedListener(listener);
        }
    }

    // adds listener to block destroyed event
    public static void AddBlockDestroyedListener(UnityAction listener)
    {
        // add listener to the list and to all invokers
        blockDestroyedListeners.Add(listener);
        foreach (Block invoker in blockDestroyedInvokers)
        {
            invoker.AddBlockDestroyedListener(listener);
        }
    }

    #endregion
}
