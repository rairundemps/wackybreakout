﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeInput : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        // pause the game on escape key
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MenuManager.GoToMenu(MenuName.Pause);
        }
    }
}
