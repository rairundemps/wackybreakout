﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    // score on screen support
    Text scoreText;
    int score;

    // balls left on screen support
    Text ballsLeftText;
    int ballsLeft;

    // last ball lost event support
    LastBallLostEvent lastBallLostEvent = new LastBallLostEvent();

    // Start is called before the first frame update
    void Start()
    {
        // populate fields and text for the score and number of balls 
        scoreText = GameObject.FindWithTag("Score").GetComponent<Text>();
        score = 0;
        scoreText.text = "Score : " + score;

        ballsLeftText = GameObject.FindWithTag("BallsLeft").GetComponent<Text>();
        ballsLeft = ConfigurationUtils.BallsPerGame;
        ballsLeftText.text = "Balls left: " + ballsLeft;

        // set listener for the points added and ball decrease events
        EventManager.AddPointsAddedListener(UpdateScore);
        EventManager.AddBallsDecreaseListener(DecreaseBallsLeft);

        // set the invoker for the last ball lost event
        EventManager.AddLastBallLostInvoker(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    // updates score and respective text in the HUD
    void UpdateScore(int points)
    {
        score += points;
        scoreText.text = "Score: " + score.ToString();
    }

    // decreases number of balls left to spawn and respective text in the HUD
    void DecreaseBallsLeft()
    {
        if (ballsLeft == 0)
        {
            lastBallLostEvent.Invoke();
        }
        else
        {
            AudioManager.Play(AudioClipName.BallLost);
            ballsLeft -= 1;
            ballsLeftText.text = "Balls left: " + ballsLeft;
        }
    }

    public void AddLastBallLostEventListener(UnityAction listener)
    {
        lastBallLostEvent.AddListener(listener);
    }
}
