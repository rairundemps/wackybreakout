﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverListener : MonoBehaviour
{
    // game 
    GameObject gameOverMenu;

    // Start is called before the first frame update
    void Start()
    {
        // add listeners for last ball lost and block destroyed events
        EventManager.AddLastBallLostListener(GameOverMenuLoader);
        EventManager.AddBlockDestroyedListener(GameOverChecker);
    }

    void GameOverMenuLoader()
    {
        AudioManager.Play(AudioClipName.GameOver);
        MenuManager.GoToMenu(MenuName.GameOver);
    }

    void GameOverChecker()
    {
        if (GameObject.FindGameObjectsWithTag("Block").Length == 1)
        {
            GameOverMenuLoader();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
