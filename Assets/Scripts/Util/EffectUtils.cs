﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EffectUtils
{
    #region Fields

    static SpeedupEffectMonitor speedupEffectMonitor;

    #endregion

    #region Properties

    // get if speedup effect is active or not
    public static bool SpeedupIsActive
    {
        get { return speedupEffectMonitor.IsActive; }
    }

    // get what speedup factor is

    // get how much time of speedup effect is left
    public static float SpeedupTimeLeft
    {
        get { return speedupEffectMonitor.TimeLeft; }
    }

    #endregion

    public static void Initialize()
    {
        speedupEffectMonitor = Camera.main.GetComponent<SpeedupEffectMonitor>();
    }
}
